import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { MoviesService } from '../movies.service';
import { IMovies } from '../movie-list/movies';


@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {

  public movieId;
  public movie: IMovies;
  private movieTitle = 'People';


  constructor(private movieService: MoviesService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.movieId = parseInt(params.get('id'));
      if (this.movieId >= 10) { this.movieId = 0; }

      this.movieTitle = params.get('filter');

      this.loadMovie(this.movieId);
    });

  }

  private loadMovie(id) {

    this
      .movieService
      .getRemoteMovies(this.movieTitle)
      .subscribe( data => {
        this.movie = data['Search'][this.movieId];

        console.log('Title: ' + this.movie.Title);
      });
  }

  goPrevious() {
    let previousId = this.movieId - 1;
    if (this.movieId < 0) { previousId = 0; }
    this.router.navigate(['/movies', previousId]);
  }
  goNext() {
    let nextId = this.movieId + 1;
    if (this.movieId >= 10) { nextId = 0; }
    this.router.navigate(['/movies', nextId]);
  }

  gotoMovieList() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

}
