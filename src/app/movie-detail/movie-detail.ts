export interface IMovieDetail {
  Title: string;
  Year: number;
  Type: string;
  Poster: string;
  Plot: string;
  imdbID: number;
  Runtime: number;
  Genre: string;
}
