export interface IMovies {
  Title: string;
  Year: number;
  Type: string;
  Poster: string;
}
