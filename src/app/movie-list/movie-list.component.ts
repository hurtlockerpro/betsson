import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {MoviesService} from '../movies.service';


@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
})
export class MovieListComponent implements OnInit {

  public form: {
    filter: string;
  };
  totalRec = 0;
  page = 1;
  public movies = [];
  private movieTitle = 'People';

  constructor(private movieService: MoviesService, private router: Router, private route: ActivatedRoute) {
    this.form = {
      filter: ( route.snapshot.params.filter || '' )
    };
    this.movies = new Array<any>();
  }

  public applyFilter(): void {

    // apply filter
    this.applyFilterToRoute();

    // do the serach again
    this.loadMovies();

  }

  ngOnInit(): void {

    this.route.paramMap.subscribe((params: ParamMap) => {
      const searchTitle: string = params.get('filter');
      this.movieTitle = searchTitle;
    });

    this.loadMovies();
  }

  private loadMovies() {

    this
      .movieService
      .getRemoteMovies(this.movieTitle)
      .subscribe( data => {
        this.movies = data['Search'];

        if (typeof this.movies !== 'undefined') { this.totalRec = this.movies.length; }
        // console.log(this.totalRec);
        // console.log(this.page);
        console.log ('movieTitle: ' + this.movieTitle);
      });
  }

  showDetails(id: number) {
    this.router.navigate([id], { relativeTo: this.route });
  }


  // apply the filter
  private applyFilterToRoute(): void {

    this.router.navigate(
      [
        {
          filter: this.form.filter
        }
      ],
      {
        relativeTo: this.route,
        replaceUrl: true
      }
    );
    document.title = `Search: ${ this.form.filter }`;

    // save input value
    this.movieTitle = this.form.filter;
  }

}
