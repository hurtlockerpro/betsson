
import {MoviesService} from '../movies.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('MovieService test', () => {

  let movieService: MoviesService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MoviesService],
      imports: [HttpClientTestingModule]
    });

    httpTestingController = TestBed.get(HttpTestingController);
    movieService = TestBed.get(MoviesService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created MovieService', () => {
    expect(movieService).toBeTruthy();

    const mockMovies  = [
      { Title: '24 Hour Party People',
        Year: 2002
      },
      { Title: '24 Hour Party People: The Factory Records Saga',
        description: 2002
      }
    ];

    movieService.getRemoteMovies('24 people').subscribe( data => {
      expect(data[0].Title).toEqual('24 Hour Party People');
    });

    const req = httpTestingController.expectOne(
      'http://www.omdbapi.com/?apikey=f79aeba3&s=24 people'
    );

    req.flush(mockMovies);

  });
});
