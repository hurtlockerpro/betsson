import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {IMovies} from './movie-list/movies';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import { throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(private movieService: HttpClient) {}

  getMovies() {
    return ['movie1', 'movie2', 'movie3'];
  }

  getRemoteMovies(movieTitle: string): Observable <IMovies[]> {
    return this.movieService.get<IMovies[]>('http://www.omdbapi.com/?apikey=f79aeba3&s=' + movieTitle).pipe(
      retry(3),
      catchError(err => { return this.errorHandler(err); } )
    );
  }

  errorHandler(error: HttpErrorResponse) {
    console.log('error occured');
    return throwError(error.message);
  }
}
