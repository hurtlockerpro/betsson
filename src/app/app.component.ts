import { Component } from '@angular/core';
import {RouterOutlet} from '@angular/router';
import { fader } from './route-animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [ // <-- add your animations here
     fader,
    // slider,
    // transformer,

  ]
})
export class AppComponent {
  title = 'hello-world';

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
}
