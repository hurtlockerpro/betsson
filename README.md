# Test for apply for a job in Betsson Group 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.23.

Technologies used: <br>
    - [Angular](https://angular.io/)<br>
	- [TypeScript](https://www.typescriptlang.org/)<br>
	- [Rxjs](https://github.com/ReactiveX/rxjs)<br>
	- [Sass](http://sass-lang.com/)<br>
	
App has unit test inside Run `ng test` to execute the unit tests.  
App has one animation effect 
	
## How to install/build app

Run `npm install`. It will install all need packages and modules to run application.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically redirect to `http://localhost:4200/movies`. If so you are in right place.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io). Only one test was made to check if HttpService is searching as expected. 

## Contact

Test for Betsson Group is done by:

Name: Vladimir Kjahrenov <br>
Tel: +372 55 621 935<br>
E-mail: info@hurtlocker.pro<br>
Skype: hurtlockerpro
